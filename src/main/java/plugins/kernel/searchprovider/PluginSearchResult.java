/*
 * Copyright (c) 2010-2024. Institut Pasteur.
 *
 * This file is part of Icy.
 * Icy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Icy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Icy. If not, see <https://www.gnu.org/licenses/>.
 */
package plugins.kernel.searchprovider;

import org.bioimageanalysis.icy.common.string.StringUtil;
import org.bioimageanalysis.icy.extension.plugin.PluginDescriptor;
import org.bioimageanalysis.icy.network.NetworkUtil;
import org.bioimageanalysis.icy.network.search.SearchResult;
import org.bioimageanalysis.icy.network.search.SearchResultProducer;
import org.bioimageanalysis.icy.network.search.SearchResultProducer.SearchWord;

import java.awt.*;
import java.util.List;

/**
 * @author Stephane Dallongeville
 * @author Thomas Musset
 */
public abstract class PluginSearchResult extends SearchResult {
    protected final PluginDescriptor plugin;
    protected final int priority;
    protected String description;

    public PluginSearchResult(final SearchResultProducer provider, final PluginDescriptor plugin, final String text, final List<SearchWord> words, final int priority) {
        super(provider);

        this.plugin = plugin;
        this.priority = priority;

        description = "";
        int wi = 0;
        while (StringUtil.isEmpty(description) && (wi < words.size())) {
            final SearchWord sw = words.get(wi);

            if (!sw.reject)
                // no more than 80 characters...
                description = StringUtil.trunc(text, sw.word, 80);

            wi++;
        }

        if (!StringUtil.isEmpty(description)) {
            // remove carriage return
            description = description.replace("\n", "");

            for (final SearchWord sw : words)
                // highlight search keywords (only for more than 2 characters search)
                if (!sw.reject && (sw.length() > 2))
                    description = StringUtil.htmlBoldSubstring(description, sw.word, true);
        }
    }

    public PluginDescriptor getPlugin() {
        return plugin;
    }

    @Override
    public String getTitle() {
        return plugin.getName();
    }

    @Override
    public Image getImage() {
        if (plugin.isIconLoaded())
            return plugin.getIconAsImage();

        return PluginDescriptor.DEFAULT_ICON.getImage();
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void executeAlternate() {
        final String url = plugin.getWeb();

        if (!StringUtil.isEmpty(url))
            NetworkUtil.openBrowser(url);
    }

    @Override
    public int compareTo(final SearchResult o) {
        if (o instanceof PluginSearchResult)
            return ((PluginSearchResult) o).priority - priority;

        return super.compareTo(o);
    }
}
